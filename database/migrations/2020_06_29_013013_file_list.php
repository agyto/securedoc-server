<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FileList extends Migration
{
    private $table = 'file_list';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("id_user")->unsigned();
                $table->foreign("id_user")->references("id")->on("users");
            $table->string("file_name");
            $table->string("file_uuid");
            $table->string("file_origin_name");
            $table->string("file_type");
            $table->string("file_mime_type");
            $table->string("file_extension");
            $table->integer("file_size")->unsigned();
            $table->string("file_categorise")->nullable();
            $table->enum("file_status",["approve","reject","awaiting"])->default("awaiting");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
