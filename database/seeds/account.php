<?php

use Illuminate\Database\Seeder;

class account extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            'name' => 'RE Administrator',
            'email' => 'admin@re.com',
            'password' => app('hash')->make('1234'),
            'type' => 'admin'
            ],
            [
            'name' => 'RE Employee',
            'email' => 'emp@re.com',
            'password' => app('hash')->make('1234'),
            'type' => 'employee'
            ]
        ]);
    } 
}
