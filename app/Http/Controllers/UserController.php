<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\PandoraController as Pandora;


class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth");
    }

    public function getUserDetail(Request $req)
    {

        // $newtoken = Pandora::generateNewToken();

        $data = [
            "message" => "success",
            "code"    => 200,
            "result"  => [
                "token" => Auth::user()->token,
                "data" => User::find(Auth::user()->id)
            ],
        ];

        return response()->json($data, $data["code"]);
    }
}