<?php

namespace App\Http\Controllers;

use Auth;
use Event;

use App\Filecomment;
use App\Events\PusherEvent;
// use App\Http\Controllers\PandoraController as Pandora;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class CommentController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth");
    }

    public function getComments(Request $req){

        $data = Filecomment::where('com_file_uuid', $req->_uuid)
                    ->with(array('user'=>function($query){
                        $query->select('name','id');
                    }))
                    ->orderBy('file_comments.created_at','asc')
                    ->get();

        $res = [
            "message" => "success",
            "code"    => 200,
            "result"  => [
                "data" => $data
            ]
        ];

        return response()->json($res, $res['code']);
    }

    public function addComments(Request $req){
        $newComm                    = new Filecomment();
        $newComm->id_user           = Auth::user()->id;
        $newComm->com_file_uuid     = $req->_uuid;
        $newComm->com_comment       = $req->_comment;
        $newComm->save();

        $data = Filecomment::where('com_file_uuid', $req->_uuid)
                    ->with(array('user'=>function($query){
                        $query->select('name','id');
                    }))
                    ->orderBy('file_comments.created_at','asc')
                    ->get();

        // Broadcast to client
        // $msg = [
        //     'comment' => true,
        //     '_uuid' => $newComm->_uuid,
        //     '_com' => $data
        // ];

        // event(new PusherEvent($msg));

        $res = [
            "message" => "success",
            "code"    => 200,
            "result"  => [
                "data" => $data
            ]
        ];

        return response()->json($res, $res['code']);
    }

   
}
