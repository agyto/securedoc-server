<?php

namespace App\Http\Controllers;

use Auth;
use Event;

use App\User;
use App\FileList;
use App\FileListIdx;

use App\Events\PusherEvent;
use App\Http\Controllers\PandoraController as Pandora;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Spatie\PdfToText\Pdf;

class DocumentsController extends Controller
{

    public function __construct()
    {
        // $this->middleware("auth");
    }

    public function getList(){
        if(Auth::user()->type == "employee"){
            $data = FileList::where("id_user", Auth::user()->id)
                    ->with(array('user'=>function($query){
                        $query->select('name','id','type');
                    }))
                    ->orderBy('file_list.id','desc')
                    ->get();
        } else if (Auth::user()->type == "admin"){
            $data = FileList::with(array('user'=>function($query){
                        $query->select('name','id','type');
                    }))
                    ->orderBy('file_list.id','desc')
                    ->get();
        } else {
            $data = null;
        }
        $res = [
            "message" => "success",
            "code"    => 200,
            "result"  => [
                "data" => $data
            ]
        ];

        return response()->json($res, $res['code']);
    }

    public function uploadFile(Request $req)
    {
        $file                       = $req->file('file');
        $readPdf                    = (new Pdf(env('PDFTOTEXTPATH', '/usr/local/bin/pdftotext')))->setPdf($file)->text();

        $destinationPath            = storage_path('filez');
        $fileName                   = Pandora::getRandomString(100).time().".".$file->getClientOriginalExtension();

        $uuid = Pandora::getRandomString(50, time());

        $ctime = \Carbon\Carbon::now();

        // Document status; admin = approve, employee = waitting for approvement
        if(Auth::user()->type == 'admin') {
            $docstatus = 'approve';
        } else {
            $docstatus = 'awaiting';
        }
        
        // loop only get unique uuid
        while(FileList::where('file_uuid',$uuid)->get()->first() == true){
            $uuid = Pandora::getRandomString(50, time());
        }

        $sdata                      = new FileList();
        $sdata->id_user             = Auth::user()->id;
        $sdata->file_name           = $fileName;
        $sdata->file_origin_name    = $file->getClientOriginalName();
        $sdata->file_type           = $file->getType();
        $sdata->file_mime_type      = $file->getMimeType();
        $sdata->file_extension      = $file->getClientOriginalExtension();
        $sdata->file_size           = $file->getSize();
        $sdata->file_uuid           = $uuid;
        $sdata->file_categorise     = $req->fcatagorise;
        $sdata->file_status         = $docstatus;
        $sdata->created_at          = $ctime;
        
        if($file->move($destinationPath, $fileName) && $sdata->save()){
            $res = [
                "message" => "success",
                "code"    => 200,
                "result"  => [
                    "data" => FileList::where('id',$sdata->id)
                                ->with(array('user'=>function($query){
                                    $query->select('name','id','type');
                                }))
                                ->get()
                ]
            ];
            
            // save to elasticsearch
            $doctext            = $readPdf;
            $inuser['name']     = Auth::user()->name;
            $inuser['type']     = Auth::user()->type;


            $indata = new FileListIdx();
            $indata->file_id            = $sdata->id;
            $indata->file_name          = $sdata->file_name;
            $indata->file_uuid          = $sdata->file_uuid;
            $indata->file_origin_name   = $sdata->file_origin_name;
            $indata->file_extension     = $sdata->file_extension;
            $indata->file_size          = $sdata->file_size;
            $indata->file_content       = $doctext;
            $indata->file_categorise    = $req->fcatagorise;
            $indata->file_status        = $docstatus;
            $indata->user               = $inuser;
            $indata->created_at         = $ctime;
            $indata->save();

            // Broadcast to admin update
            $msg = [
                'message' => 'A new document have been added',
                'type' => 'admin'
            ];

            event(new PusherEvent($msg));
            return response()->json($res, $res['code']);
        }         
    }

    public function downloadFile(Request $r)
    {
        // blob file response
        $file = FileList::where("file_uuid",$r->id)->get()->first();
        $urlpath = "files/".$file->file_name;   
        $filePath   = storage_path('filez/' . $file->file_name);

        $headers = [
                'Content-Type: '.$file->file_mime_type
                ];

        return response()->download($filePath, $file->file_name, $headers, 'inline');

    }

    public function searchDocument(Request $req)
    {

        if(Auth::user()->type == 'employee'){
            $query = [
                "bool" => [
                    "should" => [
                        ["match" => [ "file_content" => $req->_search ]],
                        ["match" => [ "user.name" => Auth::user()->name ]],
                        ["match" => [ "user.type" => Auth::user()->type ]]
                    ]
                ]
            ];
        } else {
            $query = [
                "match" => [ "file_content" => $req->_search ]
            ];
        }

        $data = app("es")->raw()->search([
                    "index" => "re_docsidx_index",
                    "type"  => "posts",
                    "body" => [
                        "query" => $query,
                        "sort" => [ 
                            "created_at" => "asc"
                        ],
                        "highlight" => [
                            "number_of_fragments" => 3,
                            "fragment_size" => 150,
                            "pre_tags" => ["<b>"],
                            "post_tags" => ["</b>"],
                            "fields" => [
                                "file_content" => new \stdClass()
                            ]
                        ]
                    ]
                ]);

        return $data;

    }

    public function testEvent()
    {
        $msg = [
            'message' => 'A new document have been added',
            'type' => 'admin'
        ];

        return event(new PusherEvent($msg));
    }
}


