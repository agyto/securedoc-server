<?php

namespace App\Http\Controllers;

use Auth;
use Event;
use App\User;
use Illuminate\Http\Request;
use App\FileListIdx;

class PandoraController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function msg(){
        $post = new FileListIdx;
        $post->file_id = 23;
        $post->file_name = '190283901823_klsnsdfsdf.jpeg';
        $post->file_origin_name = 'klsnsdfsdf.jpeg';
        $post->file_content = 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Consequuntur, nisi officiis minus, aliquid iure sint quo corrupti recusandae, sed earum ipsam laborum commodi expedita cumque ipsa optio dolores itaque nam.';
        // $post->save();

        return response()->json(FileListIdx::all(), 200);
    }

    public function downloadFile(Request $r)
    {
        // blob file response
        $file = FileList::find($r->id);
        $urlpath = "files/".$file->file_name;   
        $file   = storage_path('filez/' . $file->file_name);

        $headers = array(
                'Content-Type: application/pdf'
                );

        // echo $r->id;
        return response()->download($file, 'filename.pdf', $headers, 'inline');
        // return response()->json($file, 200);
    }


    public static function generateNewToken(){
        $token          = Self::generateRandomString();
        
        $user           = User::find(Auth::user()->id);
        $user->token    = $token;
        $user->save();

        return $token;
    }

    public static function getRandomString($length = 100, $time = ""){
        return Self::generateRandomString($length, $time = "");
    }

    private static function generateRandomString($length = 80, $time = "")
    {
        $char = '012345678dssd9abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.$time;
        $char_length = strlen($char);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $char[rand(0, $char_length - 1)];
        }
        return $str;
    }
}
