<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->messages = [
            'email' => 'Enter valid e-mail address!',
            'email.required' => 'Enter email address!',
            // 'password' => 'Enter password min 3 characters!',
            // 'password.required' => 'Enter email address!',
        ];
    }

    public function login(Request $req)
    {
        $this->validate($req, [
            'email' => 'required|email',
            'password' => 'required|min:3'
        ], $this->messages);
 
        $email  = $req->input("email");
        $pass   = $req->input("password");
 
        $user = User::where("email", $email)->first();
 
        if (!$user) {
            $out = [
                "message" => "Login failed, check your email and password",
                "code"    => 401,
                "result"  => [
                    "token" => null,
                ]
            ];
            return response()->json($out, $out['code']);
        }
 
        if (Hash::check($pass, $user->password)) {
            $newtoken  = $this->generateRandomString();
 
            $user->token =  $newtoken;
            $user->save();
 
            $data = [
                "message" => "success",
                "code"    => 200,
                "result"  => [
                    "token" => $newtoken,
                ]
            ];
        } else {
            $data = [
                "message" => "Login failed, check your email and password",
                "code"    => 401,
                "result"  => [
                    "token" => null,
                ]
            ];
        }
 
        return response()->json($data, $data['code']);
    }

    public function logout()
    {
        $user = User::where('token',Auth::user()->token)->first();
        $user->token = '';

        if ($user->save()) {
            $data = [
                "message" => "success",
                "code"    => 200
            ];
        } else {
            $data = [
                "message" => "Logout failed",
                "code"    => 401,
                "result"  => $user
            ];
        }
 
        return response()->json($data, $data['code']);
    }

    public function register(Request $req)
    {
        $this->validate($req, [
            'name' => 'required|max:255',
            'email' => 'required|unique:users|email|max:255',
            'password' => 'required|min:3',
            'repassword' => 'required|min:3'
        ],$this->messages);
 
        $name       = $req->input("name");
        $email      = $req->input("email");
        $password   = $req->input("password");
        $repassword = $req->input("repassword");
 
        $hash = Hash::make($password);
 
        $data = [
            "name" => $name,
            "email" => $email,
            "password" => $hash
        ];
 
        if($password !== $repassword){
            $data = [
                "message" => "retype password not same",
                "code"   => 400,
            ];
        } else if (User::create($data)) {
            $data = [
                "message" => "success",
                "code"    => 200,
            ];
        } else {
            $data = [
                "message" => "failed",
                "code"   => 401,
            ];
        }
 
        return response()->json($data, $data['code']);
    }
 
    function generateRandomString($length = 80)
    {
        $char = '012345678dssd9abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $char_length = strlen($char);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $char[rand(0, $char_length - 1)];
        }
        return $str;
    }
}
