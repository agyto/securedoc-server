<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Concerns\InteractsWithInput;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->bearerToken()) {
            $check =  User::where('token', $request->bearerToken())->first();
 
            if (!$check) {
                return response('Invalid token.', 401);
            } else {
                return $next($request);
            }
        } else {
            return response('Please input token.', 401);
        }
    }
}
