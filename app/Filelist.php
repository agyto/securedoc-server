<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class FileList extends Model
{
    protected $table = 'file_list';

    protected $hidden = [
        'id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user', 'id');
    }

    public function getCreatedAtAttribute()
    {
        \Carbon\Carbon::setLocale('id');
        return \Carbon\Carbon::parse($this->attributes['created_at'])
            ->add(7, 'hour')
            ->format('d-M-Y H:i:s A');
    }
}
