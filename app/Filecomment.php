<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Filecomment extends Model
{
    protected $table = 'file_comments';

    protected $hidden = [
        'id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user', 'id');
    }

    public function file()
    {
        return $this->belongsTo('App\Filelist', 'com_file_uuid', 'id');
    }

    public function getCreatedAtAttribute()
    {
        \Carbon\Carbon::setLocale('id');
        return \Carbon\Carbon::parse($this->attributes['created_at'])
            ->add(7, 'hour')
            ->format('d-M-Y H:i:s A');
    }
}
