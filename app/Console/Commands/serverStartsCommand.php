<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;


use App\Post;

use Exception;
use Illuminate\Console\Command;

class serverStartsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "server:start";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Start environment server";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $process = shell_exec('php -S localhost:8000 -t public/');

            $this->info("Server already started");
        } catch (Exception $e) {
            $this->error("An error occurred : ".$e);
        }
    }
}