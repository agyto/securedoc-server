<?php

namespace App;
use Basemkhirat\Elasticsearch\Model;

class FileListIdx extends Model
{
    protected $type     = "posts";
    
    protected $index    = "re_docsidx_index";

    public function getIsPublishedAttribute()
    {
        return $this->attributes['file_name'] == 1;
    }
}
