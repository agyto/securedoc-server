<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Elasticsearch Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the Elasticsearch connections below you wish
    | to use as your default connection for all work. Of course.
    |
    */

    'default' => env('ELASTIC_CONNECTION', 'default'),

    /*
    |--------------------------------------------------------------------------
    | Elasticsearch Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the Elasticsearch connections setup for your application.
    | Of course, examples of configuring each Elasticsearch platform.
    |
    */

    'connections' => [

        'default' => [

            'servers' => [

                [
                    'host' => env('ELASTIC_HOST', '127.0.0.1'),
                    'port' => env('ELASTIC_PORT', 9200),
                    'user' => env('ELASTIC_USER', ''),
                    'pass' => env('ELASTIC_PASS', ''),
                    'scheme' => env('ELASTIC_SCHEME', 'http'),
                ]

            ],

            'index' => env('ELASTIC_INDEX', 're_docsidx_index'),

            // Elasticsearch handlers
            // 'handler' => new MyCustomHandler(),
            
            'logging' => [
                'enabled'   => env('ELASTIC_LOGGING_ENABLED',false),
                'level'     => env('ELASTIC_LOGGING_LEVEL','all'),
                'location'  => env('ELASTIC_LOGGING_LOCATION',base_path('storage/logs/elasticsearch.log'))
            ],            
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Elasticsearch Indices
    |--------------------------------------------------------------------------
    |
    | Here you can define your indices, with separate settings and mappings.
    | Edit settings and mappings and run 'php artisan es:index:update' to update
    | indices on elasticsearch server.
    |
    | 'my_index' is just for test. Replace it with a real index name.
    |  support types : integer, float, double, string, boolean, object and array
    |
    | Fields :
    | file_id
    | file_name
    | file_uuid
    | file_origin_name
    | file_extension
    | file_content
    | file_categorise
    | user
    | + name
    | + type
    */

    'indices' => [

        're_docsidx' => [

            'aliases' => [
                're_docsidx_index'
            ],

            'settings' => [
                'number_of_shards' => 1,
                'number_of_replicas' => 0,
            ],

            'mappings' => [
                'posts' => [
                    'properties' => [
                        'file_id' => [
                            'type' => 'integer'
                        ],
                        'file_name' => [
                            'type' => 'text'
                        ],
                        'file_uuid' => [
                            'type' => 'text'
                        ],
                        'file_origin_name' => [
                            'type' => 'text'
                        ],
                        'file_extension' => [
                            'type' => 'text'
                        ],
                        'file_content' => [
                            'type' => 'text'
                        ],
                        'file_size' => [
                            'type' => 'integer'
                        ],
                        'file_categorise' => [
                            'type' => 'text'
                        ],
                        'file_status' => [
                            'type' => 'text'
                        ],
                        'created_at' => [
                            'type' => 'text',
                            'fielddata' => true
                        ],
                        'user' => [
                            'properties' => [
                                'name'=> [
                                    'type'=> 'text'
                                ],
                                'type'=>[
                                    'type'=> 'text'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]


    ]
];
