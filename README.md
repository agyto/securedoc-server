# RE - securedocs-server

**This repo was build to complete task force by RE**

## Requirement
- Elasticsearch
- Composer
- PHP 7.2.5 (Warning! Please set memory limit to -1 (unlimited) or more than 1611MB in php.ini to use pusher-php-server)
- Poppler 
    + win check http://blog.alivate.com.au/poppler-windows/
    + ubuntu/debian -> $ apt-get install poppler-utils 
    + centos -> $ yum install poppler-utils 

## Library Bug Fix

``` bash
Caution! For using elasticsearch > 7.3.* 
please make bug fix for vendor Basemkhirat\Elasticsearch below :

# Create index command(Basemkhirat\Elasticsearch\src\Command\CreateIndexCommand.php)
# Update index command (Basemkhirat\Elasticsearch\src\Command\UpdateIndexCommand.php)

if (isset($config['mappings'])) {
    foreach ($config['mappings'] as $type => $mapping) {
        $this->info("Creating mapping for type: {$type} in index: {$index}");
        $client->indices()->putMapping([
            'index' => $index,
            'type' => $type,
            'body' => $mapping,
            // add option below
            'custom' => [ 
                'include_type_name' => true
            ]
        ]);
    }
}
```

## Installation
``` bash
# Build setup
$ composer install

# Copy env configuration (including pusher.io account)
$ cp .env.custom .env

# Database migration & seed
$ php artisan migrate
$ php artisan seed

# Running elasticsearch (setting port in env)
$ elasticsearch

# Add new index to elasticsearch base on es config
$ php artisan es:indices:create

# Run the server
$ php artisan server:start

```

## Copyrights

The Lumen framework [MIT license](https://opensource.org/licenses/MIT).
