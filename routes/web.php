<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix'=>'api'], function() use ($router){
    $router->post("/register", "AuthController@register");
    $router->post("/login", "AuthController@login");
    $router->get("/logout", "AuthController@logout");
    $router->post("/getuser", "UserController@getUserDetail");
    $router->post("/upload", "DocumentsController@uploadFile");
    $router->get("/documents/getall", "DocumentsController@getList");
    $router->get("/download/{id}", "DocumentsController@downloadFile");
    $router->post("/comment/get", "CommentController@getComments");
    $router->post("/comment/add", "CommentController@addComments");
    $router->get("/documents/search", "DocumentsController@searchDocument");


});

$router->get("/msg", "DocumentsController@testEvent");